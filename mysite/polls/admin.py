from django.contrib import admin
from django.db.models import Count
from .models import Choice, Question, Organization, Member


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 1

class MemberInline(admin.TabularInline):
    model = Member
    extra = 1

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_filter = ['pub_date']
    search_fields = ['question_text']
    list_display = ('question_text', 'pub_date', 'choices_count', 'author', 'was_published_recently')

    def get_queryset(self, request):
        return Question.objects.annotate(choices_count=Count('choice'))

    def choices_count(self, obj):
        return obj.choices_count

    def save_model(self, request, obj, form, change):
        # Set current user as author only if new insert
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

    choices_count.short_description = 'Choices'
    choices_count.admin_order_field = 'choices_count'


class OrganizationAdmin(admin.ModelAdmin):
    inlines = [MemberInline]
    list_filter = ['cre_date', 'mod_date']
    list_display = ('name', 'description', 'url', 'mod_date', 'cre_date')

admin.site.register(Question, QuestionAdmin)
admin.site.register(Organization, OrganizationAdmin)
