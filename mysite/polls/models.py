import datetime
from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.conf import settings

class Question(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default=timezone.now())

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
        
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class Organization(models.Model):
    name = models.CharField(max_length=200)
    members = models.ManyToManyField(get_user_model(), through='Member')
    description = models.TextField(null=True, blank=True)
    url = models.CharField(max_length=150, null=True, blank=True)
    cre_date = models.DateTimeField(verbose_name='date created', auto_now_add=True)
    mod_date = models.DateTimeField(verbose_name='date modified', auto_now=True)


class Member(models.Model):
    user = models.ForeignKey(get_user_model(), null=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    joined_date = models.DateTimeField('date joined', auto_now_add=True)
